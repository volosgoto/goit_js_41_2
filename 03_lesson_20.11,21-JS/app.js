// let userName = "Vova";

// userName += "Sara";
// userName += "Bob";

// console.log(userName);

// function getSum(a, b) {
//     console.log(a + b);
// }

// getSum(5, 7);

let vova = {
    name: "Vova",
    sayHello() {
        console.log(`Hello ${this.name}`);
    },
};

let sara = {
    name: "Sara",
};

// vova.sayHello();

// call
// apply
// bind

// Вызови мне метод sayHello объекта vova в контексте объекта sara
// vova.sayHello.call(sara);

let germanCars = [
    { id: 1, title: "BMW" },
    { id: 2, title: "Audi" },
    { id: 3, title: "Opel" },
];

let japanCars = [
    { id: 1, title: "Honda" },
    { id: 2, title: "Toyota" },
    { id: 3, title: "Subaru" },
];

let carInfo = {
    getInfo() {
        console.log(this.title);
    },
};

// for (let car of cars) {
//     // console.log(car)
//     carInfo.getInfo.call(car);
// }

function getSpecs(carsArrayObj, info) {
    for (let car of carsArrayObj) {
        info.getInfo.call(car);
    }
}

getSpecs(germanCars, carInfo);
