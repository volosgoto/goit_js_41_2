// Closures

// Замыкания - это когда функции доступны вненшние переменные

// let a = 10;

// function getInfo() {
//     // let a = 20;

//     console.log(a);
// }

// getInfo();

// function mult(n) {
//     return function () {
//         return 10 * n;
//     };
// }

// let result = mult(2)() -карирование
// console.log(result);
// console.log(result());

// function addOne(n) {
//     return function (i) {
//         return n + i;
//     };
// }

// let result = addOne(10)(1) - карирование
// console.log(result);

// let result = addOne(10);
// console.log(result(1));

// function myFunc(a){

//     dsfsdfsdf
//     return function(b){
//         asdsdfsdf
//         return function(c){

//         }
//     }
// }
// myFunc(a)(b)(c) - карирование

// let japanCars = [
//     { id: 1, title: "Honda" },
//     { id: 2, title: "Toyota" },
//     { id: 3, title: "Subaru" },
// ];

// let carInfo = {
//     getInfo() {
//         console.log(this.title);
//     },
// };

// function getSpecs(carsArrayObj, info) {
//     for (let car of carsArrayObj) {
//         info.getInfo.bind(car)();
//     }
// }

// getSpecs(japanCars, carInfo);

// ==========================

function sum(a, b) {
    console.log(a + b);
}

sum(10, 20);

(function (a, b) {
    console.log(a + b);
})(10, 20);

((a, b) => {
    console.log(a + b);
})(10, 20);
