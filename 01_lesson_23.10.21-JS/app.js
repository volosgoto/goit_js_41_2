/*
String;
Number; 
    // NaN
    // Infinity


Boolean;
null;  - object
undefined;

Symbol;
BigInt;

Object;
*/

// let data = 500;
// data = undefined;
// data = null;
// data = Symbol('qwe');
// data = 23412354134324234234234234324;
// data = 23412354134324234234234234324n;
// data = {};
// data = [];
// data = null;
// data = true;
// data = NaN;
// data = Infinity;
// data = 'qweqweqw';
// let result = 'Hello ' + 'Vova' + 10 + 200;
// let result = 10 + 200;
// let result = 'Vova' / 10;
// let result = 10 + true;
// result = 10 * 'Vova';

// Type hinting
// 1. String
// 2. Number
// 3. Boolean

// Falsy values
// 0
// NaN
// null
// undefined
// '', "", ``
// false

// console.log(typeof result);
// console.log(result);

// Logical operators
// !
// <
// >
// <=
// >=
// ||
// &&
// ==
// ===
// !=
// !==

// let result = 5 < 10;
// result = 5 !== 10;
// result = 5 || 10;

// console.log(result);
// console.log(typeof result);

// if(Boolean true) {
//     console.log('Hello')
// }

// sdfsdfdsfdsf;
// sdfsDfSDklfnSNFSDN;

// let userName = '';
// if (userName) {
//     console.log(typeof 'Hello');
//     console.log('Hello');
// } else {
//     console.log('False');
// }

// '' -single qoutes
// "" - double qoutes
//  Template string
// `` - backticks

// let userName = 'Bob';
// let userAge = 25;
// let getingMsg = 'Hello' + ' ' + userName + ' you are ' + userAge + ' years';
// let getingMsg = `Hello ${userName} you are ${25} years`;

// console.log(getingMsg);

// console.log();
// console.dir();
// console.error();
// console.debug()

// let userName = 'Bob';
// window.console.log(userName);
// console.log(userName);

// let message = prompt('Enter password');

// console.log(message);

// let pass = '12345';
// let numPass = Number(pass);
// numPass = parseInt(pass);
// numPass = parseFloat(pass);

// console.log(numPass);
// console.log(typeof numPass);

// let pass = 12345;
// let strPass = String(pass);
// strPass = pass.toString();

// console.log(strPass);
// console.log(typeof strPass);

// if(Boolean true) {

// }

// if (Boolean false) {

// } else {

// }

// if () {

// } else if () {

// } else if () {

// } else if () {

// } else {

// }

// ==================================================
// Task_3

// let message = prompt('Enter user password');

// const ADMIN_PASS = 1234;
// let strAdminPass = String(ADMIN_PASS);

// if (message === strAdminPass) {
//     console.log('Hello Admin');
// } else if (message === null) {
//     console.log('Cancel pressed');
// } else {
//     console.log('Wrong Pass');
// }

// console.log(strAdminPass);
// console.log(typeof strAdminPass);

// string or null

// console.log(message);
// console.log(typeof message);

// ===================================================
// let userInput = prompt('Enter user password');

// const ADMIN_PASS = 1234;
// let strAdminPass = String(ADMIN_PASS);
// let message = '';

// if (userInput === strAdminPass) {
//     message = 'Hello Admin';
// } else if (userInput === null) {
//     message = 'Cancel pressed';
// } else {
//     message = 'Wrong Pass';
// }

// console.log(message);

// ==================================

// let cases = 26000;
// let lockDown = 30000;
// let message = '';

// let pandemicSituation =
//     cases >= lockDown ? (message = 'Stop') : (message = 'Go');
// // let pandemicSituation =
// //     cases >= lockDown ? (cases >= lockDown ? 'Stop' : 'Go') : 'Go';

// // console.log(pandemicSituation);
// console.log(message);

// // Boolean ? (Boolean ? true : false) : false;
// Boolean ? true : false;

// Switch
// let green = 'green';
// let red = 'red';
// let yellow = 'yellow';

// let ligth = yellow;

// switch (ligth) {
//     case 'green':
//         console.log('Go');
//         let userName = prompt('Enrer name');
//         console.log(userName);

//         break;

//     case 'red':
//         console.log('Stop');
//         break;

//     case 'yellow':
//         console.log('Yeild');
//         break;

//     default:
//         console.log('Traffic light is brocken');
// }

// if (ligth === 'green') {
//     console.log('Go');
// } else if (ligth === 'red') {
//     console.log('Stop');
// } else if (ligth === 'yellow') {
//     console.log('Yeild');
// } else {
//     console.log('Traffic light is brocken');
// }

// if(){}

// switch(){}

// for(){}

// while(){}

let arr = [];
gitconsole.log(arr == !arr);
console.log(typeof arr);
console.log(typeof !arr);

// let res = Boolean({});
// res = Boolean([]);

// console.log(res);
