class Menu {
    constructor(deseases) {
        this.menuBtnRef = document.querySelector("#menu");
        this.rootRef = document.querySelector("#root");
        this.deseases = deseases;
        this.ul = null;

        this.menuListItems = null;
    }

    createLi(menuItem) {
        let { name, href } = menuItem;
        let li = document.createElement("li");
        let a = document.createElement("a");

        a.setAttribute("href", href);
        a.setAttribute("target", "_blank");
        a.textContent = name;

        a.classList.add("item__link");
        li.classList.add("item__list");

        li.append(a);

        return li;
    }

    createUl(menuListItems) {
        this.ul = document.createElement("ul");
        this.ul.classList.add("menu", "hide");
        this.ul.append(...menuListItems);
    }

    createMenu() {
        this.rootRef.append(this.ul);

        let menuListItems = this.deseases.map((menuItem) => {
            return this.createLi(menuItem);
        });

        this.menuListItems = menuListItems;
        console.log(this.menuListItems);
    }

    onButtonMenuClick = () => {
        this.createMenu();

        if (!this.ul) {
            this.createUl(this.menuListItems);
        }

        this.ul.classList.toggle("show");

        if (this.ul.classList.contains("show")) {
            this.menuBtnRef.textContent = "Закрыть меню";
        } else {
            this.menuBtnRef.textContent = "Открыть меню";
        }
    };

    addListeners() {
        this.menuBtnRef.addEventListener("click", this.onButtonMenuClick);
    }

    init() {
        this.addListeners();
    }
}

const deseases = [
    {
        name: "plague",
        href: "https://en.wikipedia.org/wiki/Plague_(disease)",
    },
    {
        name: "anthrax",
        href: "https://en.wikipedia.org/wiki/Anthrax",
    },
    {
        name: "ebola",
        href: "https://en.wikipedia.org/wiki/Ebola",
    },
    {
        name: "coronavirus",
        href: "https://en.wikipedia.org/wiki/Coronavirus",
    },
];

let menu = new Menu(deseases);
menu.init();
