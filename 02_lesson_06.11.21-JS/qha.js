// Append

// let userName = "Vova";
// userName += "Sara";
// userName += "Bob";
// // userName = "Bob";
// // userName = "Mike";

// console.log(userName);

// if(){}
// for(){}
// switch(){}

function sayHello(userName = "Bob") {
    console.log(`Hello  ${userName}`);
}

// sayHello("Vova");
// sayHello("Sara");
// sayHello();
// sayHello();
// sayHello();
// sayHello();

// console.log("Hello Vova");
// console.log("Hello Sara");
// console.log("Hello Bob");

// let nums = [2, 20, -5, 30, 100, 50];
// let pizza = [10, 50, -10, 3000, 45];

// function findMin(numsArr) {
//     let min = numsArr[0];
//     let len = numsArr.length;
//     for (let i = 0; i < len; i++) {
//         if (min > numsArr[i]) {
//             min = numsArr[i];
//         }
//     }

//     return min;
// }

// findMin(nums);

// console.log(findMin(pizza));

// let result = findMin(nums);
// console.log(result);

// ===============================

// let user = {
//     name: "Vova",
//     age: 20,
// };

// console.log(user.name);
// console.log(user.age);

// user.status = "admin";
// user.name = "Sara";

// console.log(user);

// console.log(user["name"]);
// console.log(user["age"]);

// user["age"] = 30;
// user["active"] = false;

// console.log(user);

// let bank = {
//     USA: "Phizer",
//     GB: "Astra",
//     Russuia: "Spitnik -v",
// };

// let a = 10;

// function test() {
//     // let a = 20;
//     console.log(a);
// }

// test();

// let a = 10;
// let b = 20;

// console.log("a", a);
// console.log("b", b);

// {
//     let a = 10;
//     let b = 20;

//     console.log("a", a);
//     console.log("b", b);
// }

// {
//     console.log("a", a);
//     console.log("b", b);
// }

// let status = true;
// let a = 10;

// if (status) {
//     let a = 100;
//     console.log(a);
// }

// C -create
// R -read
// U -update
// D -delete

// let a = 10 //C
// a = 20 // U
// delete a // D
// console.log(a) // R

// let user = {
//     name: "Vova",
//     email: "vov@i.ua",
//     pass: 1234,
//     age: 30,
//     status: "Admin",
// };

// let guest = {
//     ...user,
//     email: "vova@gmail.com",
//     status: "Guest",
//     // pass: 9874,
// };

// console.log(guest);

// let msg = "hello";
// result = [...msg];
// console.log(result);
// result = { ...msg };
// console.log(result);
