let vova = {
    name: "Vova",
    age: 30,
};

let sara = {
    ...vova,
    name: "Sara",
    id: 1223123123132,
};

console.log(vova);
console.log(sara);
