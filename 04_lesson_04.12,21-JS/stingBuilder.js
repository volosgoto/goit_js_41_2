function StringBuilder(baseValue) {
    this.value = baseValue;
}

StringBuilder.prototype.getValue = function () {
    return this.value;
};

StringBuilder.prototype.padEnd = function (str) {
    this.value += str;
};

StringBuilder.prototype.padStart = function (str) {
    this.value = str + this.value;
};

StringBuilder.prototype.padBoth = function (str) {
    this.padStart(str);
    this.padEnd(str);
};

// Пиши код выше этой строки
const builder = new StringBuilder(".");
console.log(builder.getValue()); // '.'
builder.padStart("^");
console.log(builder.getValue()); // '^.'
builder.padEnd("^");
console.log(builder.getValue()); // '^.^'
builder.padBoth("=");
console.log(builder.getValue()); // '=^.^='

const { User } = require("../../models");
class User {
    getCurrentUser = async (req, res) => {
        const { email, subscription } = req.user;
        res.status(200).json({
            status: "success",
            code: 201,
            data: {
                user: {
                    email,
                    subscription,
                },
            },
        });
    };

    updateSubscription = async (req, res) => {
        const { _id, subscription } = req.user;
        const newSubscription = req.body.subscription;

        const user = await User.findByIdAndUpdate(
            { _id: _id },
            { subscription: newSubscription }
        );

        const subscriptionRules = ["starter", "pro", "business"];

        if (!subscriptionRules.includes(newSubscription)) {
            res.status(200).json({
                status: "success",
                code: 200,
                messsage: `Choose correct type of subscription: ${subscriptionRules})`,
            });
        }

        if (subscription === newSubscription) {
            res.status(200).json({
                status: "success",
                code: 200,
                messsage: `Previous subscription (${subscription}) is equal to a new subscription (${newSubscription})`,
            });
        }

        res.status(200).json({
            status: "success",
            code: 200,
            data: {
                messsage: `Congratulations! Subscription changed from ${subscription} to ${newSubscription}`,
            },
        });
    };
}

module.exports = new User();
