class Shop {
    #items;

    constructor(name, adress, items) {
        this.name = name;
        this.adress = adress;
        this.#items = items;
    }

    showItems() {
        for (let item of this.#items) {
            const { id, name, price, qty, category } = item;
            console.log(
                `id: ${id}, name: ${name}, price: ${price}, qty: ${qty}, category: ${category}`
            );
        }
    }

    addItem({ name, price, qty, category }) {
        let item = { name, price, qty, category };
        this.#items = [...this.#items, { ...item, id: this.#generateID() }];
    }

    updateItem(oldName, newName) {
        for (let item of this.#items) {
            if (item.name === oldName) {
                item.name = newName;
            }
        }
    }
    deleteItem(Name) {
        for (let item of this.#items) {
            if (item.name === Name) {
                let idx = this.#items.indexOf(item);

                this.#items.splice(idx, 1);
            }
        }
    }

    #generateID() {
        return Math.random().toString().slice(2);
    }
}

let items = [
    { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
    { id: "id-2", name: "potato", price: 23, qty: 875, category: "fruits" },
    { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
    { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
];

let atb = new Shop("ATB", "Kyiv", items);

// atb.addItem({
//     name: "melon",
//     price: 80,
//     qty: 800,
//     category: "fruits",
// });

// atb.updateItem("apples", "golgapples");
// atb.deleteItem("tomatoes");
atb.showItems();
