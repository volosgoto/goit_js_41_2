// [].includes()
// [].indexOf()
// [].splice()

// let nums = [10, 20, 30, 40];

// (el, idx, arr)=>{}
// function(el, idx, arr){}

// function myData(el, idx, arr) {
// console.log("element", el);
// console.log("indexes", idx);
// console.log(arr);
// }

// let resArr = nums.forEach((el, idx, arr, pizza) => {
// return el + 5;
// console.log(el + 1);
// console.log("element", el);
// console.log("indexes", idx);
// console.log(arr);
// });

// console.log(resArr);

// nums.forEach(function (el, idx, arr, pizza) {
//     // console.log("element", el);
//     // console.log("indexes", idx);
//     // console.log(arr);
// });

// nums.forEach(myData);

// let resArr = nums.map((elem, idx, arr) => {
//     // console.log(elem + 2);
//     return elem + 2;
// });

// console.log(resArr);
// console.log(resArr === nums);

// let products = [
//     { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
//     { id: "id-2", name: "potato", price: 23, qty: 875, category: "fruits" },
//     { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
//     { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
// ];

// items.forEach((elem, idx, arr) => {
//     // console.log(elem);
//     // console.log(elem.name);
//     console.log(elem.qty);
// });

// let expenseProducts = products.map((product, idx, arr) => {
//     if (product.price >= 50) {
//         return {
//             ...product,
//             // price: product.price + 50,
//             price: product.price + product.price * 0.5,
//             sale: true,
//         };
//     }
//     return product;
// });

// let users = [
//     {
//         id: "id-1",
//         name: "Vova",
//         selary: 1500,
//         workTime: 160,
//         rate: 0.1,
//         bonus: false,
//     },
//     {
//         id: "id-2",
//         name: "Sara",
//         selary: 350,
//         workTime: 200,
//         rate: 0.15,
//         bonus: false,
//     },
//     {
//         id: "id-3",
//         name: "Bob",
//         selary: 2500,
//         workTime: 160,
//         rate: 0.1,
//         bonus: false,
//     },
//     {
//         id: "id-4",
//         name: "Mike",
//         selary: 4000,
//         workTime: 185,
//         rate: 0.25,
//         bonus: false,
//     },
// ];

// let overtime = 160;
// let bonusSelaries = users.map((user) => {
//     if (user.workTime > overtime) {
//         // { id: "id-2", name: "Sara", selary: 385, workTime: 220, rate: 0.1 },
//         return {
//             ...user,
//             selary: user.selary + user.rate * user.selary,
//             bonus: true,
//         };
//     }
//     return user;
// });

// console.table(bonusSelaries);

// let superUsers = bonusSelaries.filter((user) => {
//     return !user.bonus;
// });

// let superUsers = bonusSelaries.filter((user) => user.bonus);

// console.table(superUsers);

// let total = 0;
// bonusSelaries.forEach((user) => {
//     total += user.selary;
// });

// console.log(total);

// console.table(expenseProducts);

// [].forEach()

// (elem, idx, arr)=>{ return something }
// [].map()
// [].find()
// [].filter()
// [].sort()
// [].every()
// [].some()

// --------------------------------
// acc - ""
// acc - "pizza"
// acc - 0
// acc - []
// acc - {}
// acc - undefined
// acc - null
// acc - 500

// ---------------------------------
// [].reduce((acc, elemm inx, arr)=>{
// return acc + что-то еще
// }, acc)

// let nums = [10, 20, 30, 40];

// let total = nums.reduce((acc, elem, idx, arr) => {
//     // console.log("accumulator", acc);
//     console.log("elem", elem, "acc", acc);
//     return acc + 1;
// }, 100);

// let total = nums.reduce((acc, elem, idx, arr) => {
//     return acc + elem;
// }, 0);

// console.log(total);

// let users = [
//     {
//         id: "id-1",
//         name: "Vova",
//         selary: 1500,
//     },
//     {
//         id: "id-2",
//         name: "Sara",
//         selary: 350,
//     },
//     {
//         id: "id-3",
//         name: "Bob",
//         selary: 2500,
//     },
//     {
//         id: "id-4",
//         name: "Mike",
//         selary: 4000,
//     },
// ];

// let totalSalary = users.reduce((total, user) => {
//     return total + user.selary;
// }, 0);

// console.log(totalSalary);

let cart = [
    { id: "id-1", title: "Samsumg", price: 800, qty: 2 },
    { id: "id-2", title: "Apple", price: 1200, qty: 6 },
    { id: "id-3", title: "LG", price: 250, qty: 3 },
];

// 800 * 2 + 1200 * 6 + 250 * 3

// let allGiftsPrice = cart.reduce((sum, phone) => {
//     return sum + phone.price * phone.qty;
// }, 0);

let sale = cart.reduce((cartSale, phone) => {
    let item = { ...phone, price: phone.price + 50, complete: false };
    cartSale.push(item);
    return cartSale;
}, []);

console.table(sale);
